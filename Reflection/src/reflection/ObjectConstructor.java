package reflection;

import java.util.Collection;

public class ObjectConstructor {

	private String nome;
	private Collection<ObjectParam> objectsParam;

	public ObjectConstructor(String nome, Collection<ObjectParam> objectsParam) {
		super();
		this.nome = nome;
		this.objectsParam = objectsParam;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Collection<ObjectParam> getObjectsParam() {
		return objectsParam;
	}

	public void setObjectsParam(Collection<ObjectParam> objectsParam) {
		this.objectsParam = objectsParam;
	}

}
