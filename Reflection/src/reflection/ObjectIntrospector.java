package reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class ObjectIntrospector {

	private ObjectClass objectClass;
	private Collection<ObjectField> objectsField;
	private Collection<ObjectConstructor> objectsConstructor;
	private Collection<ObjectMethod> objectsMethod;

	private ObjectIntrospector(ObjectClass objectClass, Collection<ObjectField> objectsField,
			Collection<ObjectConstructor> objectsConstructor, Collection<ObjectMethod> objectsMethod) {
		super();
		this.objectClass = objectClass;
		this.objectsField = objectsField;
		this.objectsConstructor = objectsConstructor;
		this.objectsMethod = objectsMethod;
	}

	public ObjectClass getObjectClass() {
		return objectClass;
	}

	public void setObjectClass(ObjectClass objectClass) {
		this.objectClass = objectClass;
	}

	public Collection<ObjectField> getObjectsField() {
		return objectsField;
	}

	public void setObjectsField(Collection<ObjectField> objectsField) {
		this.objectsField = objectsField;
	}

	public Collection<ObjectConstructor> getObjectsConstructor() {
		return objectsConstructor;
	}

	public void setObjectsConstructor(Collection<ObjectConstructor> objectsConstructor) {
		this.objectsConstructor = objectsConstructor;
	}

	public Collection<ObjectMethod> getObjectsMethod() {
		return objectsMethod;
	}

	public void setObjectsMethod(Collection<ObjectMethod> objectsMethod) {
		this.objectsMethod = objectsMethod;
	}

	public static ObjectIntrospector introspector(Object object) {

		/*
		 * Recupero la classe in generale
		 */

		Class<?> classeRecuperata = object.getClass();
		// classeRecuperata � la classe dell'oggetto "object", quindi "object" � stato istanziato
		// dalla classe recuperata, cio� clazz

		// sto creando una istanza della classe ObjectClass che al suo interno possiede
		// l'unico campo "nome"
		ObjectClass objectClass = new ObjectClass(classeRecuperata.getName());
		
		/*
		 * Recupero i CAMPI (FIELDS) della classe
		 */

		// preparo il carrello vuoto per mettere dentro gli elementi ObjectField
		Collection<ObjectField> objectsField = new ArrayList<ObjectField>();

		Field[] fields = classeRecuperata.getFields();

		// ciclo sugli elementi
		for (Field field : fields) {

			String nomeField = field.getName();
			String tipoField = field.getType().getName();

			// creo istanza della classe ObjectField
			ObjectField objectField = new ObjectField(nomeField, tipoField);

			// inserisco l'istanza nel mio suddetto carrello
			objectsField.add(objectField);

		}
		
		/**
		 * Fabio � bellissimo
		 */

		/*
		 * Recupero i COSTRUTTORI (COSTRUCTORS) della classe
		 */

		// preparo il carrello dei costruttori
		Collection<ObjectConstructor> objectsConstructor = new ArrayList<ObjectConstructor>();

		// recupero i costruttori dalla classe
		Constructor<?>[] constructors = classeRecuperata.getConstructors();

		// ciclo sui costruttori recuperati
		for (Constructor<?> constructor : constructors) {

			// recupero il nome del costruttore ciclato
			String nomeConstructor = constructor.getName();

			// creo un carrello vuoto per i parametri del costruttore ciclato
			Collection<ObjectParam> parametriDelCostruttore = new ArrayList<ObjectParam>();

			// recupero i parametri del costruttore ciclato
			Parameter[] parametersConstructor = constructor.getParameters();

			// ciclo sui parametri del costruttore ciclato
			for (Parameter parameter : parametersConstructor) {

				String nomeParametro = parameter.getName();
				String tipoRitornoParametro = parameter.getType().getName();

				ObjectParam objectParam = new ObjectParam(nomeParametro, tipoRitornoParametro);

				parametriDelCostruttore.add(objectParam);
			}

			ObjectConstructor objectConstructor = new ObjectConstructor(nomeConstructor, parametriDelCostruttore);

			objectsConstructor.add(objectConstructor);
		}

		/*
		 * Recupero i metodi della classe
		 */

		// creo il carrello vuoto per i metodi
		Collection<ObjectMethod> objectsMethod = new ArrayList<ObjectMethod>();

		// recupero i metodi
		Method[] methods = classeRecuperata.getMethods();

		for (Method method : methods) {

			String nomeMetodo = method.getName();
			String tipoRitornoMetodo = method.getReturnType().getName();

			// creo un carrello vuoto per i parametri del metodo ciclato
			Collection<ObjectParam> parametriDelMetodo = new ArrayList<ObjectParam>();

			// recupero i parametri del metodo ciclato
			Parameter[] parametersMethod = method.getParameters();

			// ciclo sui parametri del metodo ciclato
			for (Parameter parameter : parametersMethod) {

				String nomeParametro = parameter.getName();
				String tipoRitornoParametro = parameter.getType().getName();

				ObjectParam objectParam = new ObjectParam(nomeParametro, tipoRitornoParametro);

				parametriDelMetodo.add(objectParam);
			}
			
			ObjectMethod objectMethod = new ObjectMethod(nomeMetodo, parametriDelMetodo, tipoRitornoMetodo);

			objectsMethod.add(objectMethod);
		}

		//creo un oggetto introspector e lo restituisco
		return new ObjectIntrospector(objectClass, objectsField, objectsConstructor, objectsMethod);

	}

}
