package reflection;

public class ObjectField {

	private String nome;
	private String tipoDelCampo;

	public ObjectField(String nome, String tipoDelCampo) {
		super();
		this.nome = nome;
		this.tipoDelCampo = tipoDelCampo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoDelCampo() {
		return tipoDelCampo;
	}

	public void setTipoDelCampo(String tipoDelCampo) {
		this.tipoDelCampo = tipoDelCampo;
	}

}
