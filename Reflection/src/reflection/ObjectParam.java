package reflection;

public class ObjectParam {

	private String nome;
	private String tipoDiRitorno;

	public ObjectParam(String nome, String tipoDiRitorno) {
		super();
		this.nome = nome;
		this.tipoDiRitorno = tipoDiRitorno;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTipoDiRitorno() {
		return tipoDiRitorno;
	}

	public void setTipoDiRitorno(String tipoDiRitorno) {
		this.tipoDiRitorno = tipoDiRitorno;
	}

}
