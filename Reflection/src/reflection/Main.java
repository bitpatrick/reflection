package reflection;

public class Main {

	public static void main(String[] args) {

		Object obj = new ObjectClass("oggetto");
		
		ObjectIntrospector objectIntrospector = ObjectIntrospector.introspector(obj);
		
		objectIntrospector.getObjectsMethod().stream().map( o -> o.getNome()).forEach(System.out::println);
		
	}

}
