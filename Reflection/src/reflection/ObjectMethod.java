package reflection;

import java.util.Collection;

public class ObjectMethod {

	private String nome;
	private Collection<ObjectParam> objectsParam;
	private String tipoRitornoMetodo;

	public ObjectMethod(String nome, Collection<ObjectParam> objectsParam, String tipoRitornoMetodo) {
		super();
		this.nome = nome;
		this.objectsParam = objectsParam;
		this.tipoRitornoMetodo = tipoRitornoMetodo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Collection<ObjectParam> getObjectsParam() {
		return objectsParam;
	}

	public void setObjectsParam(Collection<ObjectParam> objectsParam) {
		this.objectsParam = objectsParam;
	}

	public String getTipoRitornoMetodo() {
		return tipoRitornoMetodo;
	}

	public void setTipoRitornoMetodo(String tipoRitornoMetodo) {
		this.tipoRitornoMetodo = tipoRitornoMetodo;
	}

}
